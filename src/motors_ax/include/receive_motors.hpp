#ifndef RECEIVE_MOTORS_H
#define RECEIVE_MOTORS_H

#include <../../dynamixel_workbench_toolbox/include/dynamixel_workbench_toolbox/dynamixel_workbench.h>
#include <ros/ros.h>
#include <motors_msgs/motors_msgs.h>
#include <centraliza/Output.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>
#include<vector>

class receive_motors
{   
    public:
    receive_motors(const ros::NodeHandle nh_);
    ~receive_motors();

        //methods
    void subCallback(const centraliza::Output::ConstPtr& msg);
    void position_zero(const std_msgs::Empty::ConstPtr& ret);
    
    private:
    ros::NodeHandle nh;

 // Variables
    float horiz;
    float vert;
    int32_t intHoriz;
    int32_t intVert;
    bool Move;
    bool get_first_pos;
    bool return_zero = false;
    int32_t get_data[2];
    // const char *port_name = "/dev/ttyUSB0";
    // int baud_rate = 1000000;
    // uint16_t model_number = 0;
    // uint8_t dxl_id[2] = {6,2};

    // DynamixelWorkbench dxl_wb;
    bool result;


    //Subscribe
    ros::Subscriber subs;
    ros::Subscriber ret_0;

    //Publisher
    ros::Publisher publisher;

    //method
    void move_motors();
    void swap(int32_t *array);
    void answer(bool msg_movement);
    // std::vector<int32_t> read_motors();
      
   





};

#endif