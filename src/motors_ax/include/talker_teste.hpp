#ifndef TALKER_TESTE_H
#define TALKER_TESTE_H

//incluir as menssagens aqui
#include <ros/ros.h>
#include <centraliza/Output.h>

class talker_teste
{   public:
    talker_teste(ros::NodeHandle nh_);
    ~talker_teste();

    ros::Publisher publisher;


    private:
    ros::NodeHandle nh;

    //Publisher

    //Timer
    ros::Timer runTimer;
    
    //methods
    void sendMsg();
};

#endif
    