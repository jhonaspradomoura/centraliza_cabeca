#include "receive_motors.hpp"
#include <sstream>
#include <vector>
#include <iostream>
#include <std_msgs/Bool.h>
#include <std_msgs/Empty.h>
#include <../../dynamixel_workbench_toolbox/include/dynamixel_workbench_toolbox/dynamixel_workbench.h>


receive_motors::receive_motors(ros::NodeHandle _nh)
{

<<<<<<< HEAD
  subs = nh.subscribe<centraliza::Output>("network", 1, &receive_motors::subCallback, this);
  ret_0 = nh.subscribe<std_msgs::Empty>("motor_return_origin", 1, &receive_motors::position_zero, this);
  publisher = nh.advertise<std_msgs::Bool>("mov_status",1000);
=======
    nh = _nh;

    subs      = nh.subscribe<centraliza::Output>("network", 1, &receive_motors::subCallback, this);
    ret_0     = nh.subscribe<std_msgs::Empty>("motor_return_origin", 1, &receive_motors::position_zero, this);
    publisher = nh.advertise<std_msgs::Bool>("mov_status", 1000);
>>>>>>> f851d6e42a051a8f704450565e82edf8669fc189


    // Variables
    get_first_pos = true;
    intHoriz      = 0;
    intVert       = 0;
    Move          = false;
    return_zero   = false;
    result        = false;


}

receive_motors::~receive_motors()
{
}

void receive_motors::subCallback(const centraliza::Output::ConstPtr &msg)
{

    horiz    = 200 - (200 * (msg->horiz));
    vert     = 300 - (100 * (msg->vert));
    intHoriz = int(horiz);
    intVert  = int(vert);
    move_motors();
    ROS_INFO("I heard: [%d]", intHoriz);
}

void receive_motors::position_zero(const std_msgs::Empty::ConstPtr &ret)
{
    return_zero = true;
    move_motors();
}

void receive_motors::move_motors()
{

    const char *port_name   = "/dev/ttyUSB0";
    int        baud_rate    = 1000000;
    uint16_t   model_number = 0;
    uint8_t    dxl_id[2]    = {6, 2};


    DynamixelWorkbench dxl_wb;

    const char *log;

    result = dxl_wb.init(port_name, baud_rate, &log);

    if (result == false)
    {
        printf("%s\n", log);
        printf("Failed to init\n");
    }
    else
        printf("Succeed to init(%d)\n", baud_rate);

    for (int cnt = 0; cnt < 2; cnt++)
    {
        result = dxl_wb.ping(dxl_id[cnt], &model_number, &log);
        if (result == false)
        {
            printf("%s\n", log);
            printf("Failed to ping\n");
        }
        else
        {
            printf("Succeeded to ping\n");
            printf("id : %d, model_number : %d\n", dxl_id[cnt], model_number);
        }

        result = dxl_wb.jointMode(dxl_id[cnt], 70, 0, &log);
        if (result == false)
        {
            printf("%s\n", log);
            printf("Failed to change joint mode\n");
        }
        else
        {
            printf("Succeed to change joint mode\n");
        }

    }

    result = dxl_wb.addSyncWriteHandler(dxl_id[0], "Goal_Position", &log);
    if (result == false)
    {
        printf("%s\n", log);
        printf("Failed to add sync write handler\n");
    }

    //Toda vez que o manager solicitar ele volta pra posição inicial
    if (return_zero)
    {
        intHoriz = get_data[0];
        intVert  = get_data[1];

        return_zero = false;
    }

    // IF para pegar a posição inicial
    if (get_first_pos)
    {
        result = dxl_wb.itemRead(dxl_id[0], "Present_Position", &get_data[0], &log);
        if (result == false)
        {
            printf("%s\n", log);
            printf("Failed to get present position\n");
        }
        else
        {
            printf("Succeed to get present position(value : %d)\n", get_data[0]);
        }

        result = dxl_wb.itemRead(dxl_id[1], "Present_Position", &get_data[1], &log);
        if (result == false)
        {
            printf("%s\n", log);
            printf("Failed to get present position\n");
        }
        else
        {
            printf("Succeed to get present position(value : %d)\n", get_data[1]);
        }

        intHoriz = get_data[0];
        intVert  = get_data[1];

        get_first_pos = false;
    }


    int32_t       goal_position[2] = {intHoriz, intVert};
    const uint8_t handler_index    = 0;
    while (1)
    {
        ROS_INFO("os valores horiz: %d , vert: %d \n", intHoriz, intVert);
        result = dxl_wb.syncWrite(handler_index, &goal_position[0], &log);
        if (result == false)
        {
            printf("%s\n", log);
            printf("Failed to sync write position\n");
            Move = false;
        }
        sleep(3);
        Move = true;
        answer(Move);
        break;
        // swap(goal_position);
    }

}

void receive_motors::swap(int32_t *array)
{
    int32_t tmp = array[0];
    array[0] = array[1];
    array[1] = tmp;
}

void receive_motors::answer(bool msg_movement)
{
    std_msgs::Bool msg;
    msg.data = msg_movement;
    publisher.publish(msg);

}

// std::vector<int32_t> receive_motors::read_motors()
// {
//   const char *log1;
//  std::vector<int32_t> vet_ret;
//  int32_t get_data[2] = {};


//   result = dxl_wb.itemRead(dxl_id[0], "Present_Position", &get_data[0], &log1);
//   if (result == false)
//   {
//     printf("%s\n", log1);
//     printf("Failed to get present position\n");
//   }
//   else
//   {
//     printf("Succeed to get present position(value : %d)\n", get_data[0]);
//   }

//   result = dxl_wb.itemRead(dxl_id[1], "Present_Position", &get_data[1], &log1);
//   if (result == false)
//   {
//     printf("%s\n", log1);
//     printf("Failed to get present position\n");
//   }
//   else
//   {
//     printf("Succeed to get present position(value : %d)\n", get_data[1]);
//   }
//   vet_ret.push_back(get_data[0]);
//   vet_ret.push_back(get_data[1]);
//   return vet_ret;

// }



