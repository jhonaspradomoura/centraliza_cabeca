#include <talker_teste.hpp>
#include "ros/ros.h"	
#include <std_msgs/String.h>
#include <sstream>


int main(int argc, char **argv)			
{						
    ros::init (argc, argv, "talkerTesteNode");	
    ros::NodeHandle nh;				
    talker_teste talker_teste_node(nh);
    ros::spin();						
    return 0;						
}