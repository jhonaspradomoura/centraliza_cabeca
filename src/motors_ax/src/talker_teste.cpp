			
#include "talker_teste.hpp"	
#include <sstream>


talker_teste::talker_teste(ros::NodeHandle _nh)				
{												
    nh        =_nh;
    publisher = nh.advertise<centraliza::Output>("network",1000);
	sleep(1);
    sendMsg();
}

talker_teste::~talker_teste()
{
}

void talker_teste::sendMsg()
{
    centraliza::Output msg;
    msg.horiz = -0.6;
    msg.vert = -0.2;
    publisher.publish(msg);
	ros::spinOnce();
    ROS_INFO("Publishered: %f,%f",msg.horiz,msg.vert);
}
