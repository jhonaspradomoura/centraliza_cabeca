#include <receive_motors.hpp>
#include "ros/ros.h"					

int main(int argc, char **argv)			
{							
    ros::init (argc, argv, "receiveMotorsNode");	
    ros::NodeHandle nh;				
    receive_motors receive_motors_node(nh);
    ros::spin();						
    return 0;						
}