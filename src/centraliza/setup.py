from distutils.core import setup

from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
setup_args = generate_distutils_setup(packages=['network', 'video'], package_dir={'': 'src'},
									  scripts=['bin/manager.py', 'bin/camera_node.py'])
setup(**setup_args)
