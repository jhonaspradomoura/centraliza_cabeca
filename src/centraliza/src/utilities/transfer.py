from math import exp


class Linear(object):
	def __init__(self):
		pass
	
	@staticmethod
	def forward(u):
		return u
	
	@staticmethod
	def backward(u):
		return 1


class Relu(Linear):
	def __init__(self):
		super().__init__()
	
	def forward(self, u):
		return max(0, u)
	
	def backward(self, u):
		if u < 0:
			return 0
		else:
			return 1


class Sigmoid(Linear):
	def __init__(self):
		super().__init__()
	
	def forward(self, u):
		return 1 / (1 + exp(-u))
	
	def backward(self, u):
		return exp(-u) / (1 + exp(-u)) ** 2


class NSigmoid(Sigmoid):
	def __init__(self, interval):
		super().__init__()
		assert isinstance(interval, tuple) and len(interval) == 2, f'interval deve ter tamanho 2 e ser uma tuple, ' \
			f'e nao um {type(interval)}'
		self._interval = interval[1] - interval[0]
		self._lower = interval[0]
	
	def forward(self, u):
		return 1 / (1 + exp(-u)) * self._interval + self._lower
	
	def backward(self, u):
		return self._interval * exp(-u) / (1 + exp(-u)) ** 2


class Tanh(Linear):
	def __init__(self):
		super(Tanh, self).__init__()
	
	def forward(self, u):
		return 2 / (1 + exp(-2 * u)) - 1
	
	def backward(self, u):
		return 4 * exp(-2 * u) / (1 + exp(-2 * u)) ** 2
