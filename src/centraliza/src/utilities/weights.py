import numpy as np

from neurones.generic import Neuron


class Weight:
	def __init__(self, origin: Neuron, dest: Neuron, neuron_id_origin, neuron_id_dest, random=True):
		self._id = f'w_{neuron_id_origin, neuron_id_dest}'
		
		print(f'Criando conexao entre {neuron_id_origin} e {neuron_id_dest}')
		
		if random:
			self._value_h = np.random.uniform(-1, 1)
			self._value_v = np.random.uniform(-1, 1)
		else:
			self._value_h = 1
			self._value_v = 1
		
		self._origin = origin
		self._dest = dest
		
		self._data = self._origin.data
		self._origin.add_forward_weight(self)
		self._dest.add_origin_weight(self)
	
	def __str__(self):
		return f'{self._value_h}, {self._value_v}'
	
	def __mul__(self, other):
		if isinstance(other, tuple):
			return self._value_h * other[0], self._value_v * other[1]
		else:
			return self._value_h * other, self._value_v * other
	
	@property
	def value(self):
		return self._value_h, self._value_v
	
	@value.setter
	def value(self, val):
		assert isinstance(val, tuple), f'val deve ser uma tuple, e nao um {type(val)}'
		assert len(val) == 2, f'val deve ser uma tuple de tamanho 2, e nao de tamanho {len(val)}.'
		self._value_h = val[0]
		self._value_v = val[1]
	
	@property
	def data(self):
		return self._data
	
	@data.setter
	def data(self, val):
		assert isinstance(val, tuple), f'val deve ser uma tuple, e nao um {type(val)}'
		assert len(val) == 2, f'val deve ser uma tuple de tamanho 2, e nao de tamanho {len(val)}.'
		self._data = val
	
	def forward(self):
		self._data = self._origin.data
		self._dest.objective = self._origin.objective
		return self._dest.activation_function
	
	def backward(self):
		self._value_h += self.data[0] * self._dest.gradient[0]
		self._value_v += self.data[1] * self._dest.gradient[1]
	
	def effect_on_fwd_neurones(self):
		# Operação retorna tupla de valores multiplicados pelo gradiente
		weighted_gradient = self * self._dest.gradient
		return weighted_gradient
