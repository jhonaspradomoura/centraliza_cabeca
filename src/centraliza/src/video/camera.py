import json
import multiprocessing
import os

import centraliza.msg
import cv2
import numpy as np
import rospy
import std_msgs.msg
from cv_bridge import CvBridge


class Camera(object):
    def __init__(self):
        rospy.init_node('camera', anonymous=True, log_level=rospy.DEBUG)

        self._pub_img = rospy.Publisher('camera_fb', centraliza.msg.ObjectPos, queue_size=100,
                                        tcp_nodelay=True)
        self._sub_img = rospy.Subscriber('camera_cb', std_msgs.msg.Empty, callback=self.call, queue_size=10)
        self.bridge = CvBridge()
        self._lock = multiprocessing.Lock()

        self._frame, self._mask_frame = None, None
        self._cap = None
        self._object_x, self._object_y = -640, -480

        self._low_h, self._low_s, self._low_v = 0, 0, 0
        self._high_h, self._high_s, self._high_v = 255, 255, 255
        self._trackbar_startup = False

    # self.open(video='samples/sample1.mp4')

    def open(self, video=None):
        if video:
            self._cap = cv2.VideoCapture(video)
        else:
            camera = 1
            while not rospy.is_shutdown():
                try:
                    self._cap = cv2.VideoCapture(camera)
                    if self._cap is None or not self._cap.isOpened():
                        raise IOError('Failure to open capture.')
                    break
                except IOError:
                    camera = camera + 1
                    if camera == 10:
                        camera = 0
                    self._cap.release()

    def read(self, mode):
        if mode is 'train':
            self.train_read()
        else:
            self.read_file()
            while not rospy.is_shutdown():
                ret, self._frame = self._cap.read()
                if self._frame is not None:
                    self._frame = cv2.resize(self._frame, (640, 480))

                cvt_frame = cv2.cvtColor(self._frame, cv2.COLOR_BGR2HSV)

                min_hsv = np.array([self._low_h, self._low_s, self._low_v], np.uint8)
                max_hsv = np.array([self._high_h, self._high_s, self._high_v], np.uint8)
                self._mask_frame = cv2.inRange(cvt_frame, min_hsv, max_hsv)
                cv2.waitKey(1)
                self.get_object()

    def train_read(self):
        key_reader = {32: 'Toggle_pause', 27: 'Exit', 13: 'Parametros', 115: 'Salvar', 114: 'Ler', 100: 'Switch'}
        pause, ret, t_parameters, savable, switch = False, False, False, False, False
        while not rospy.is_shutdown():
            if not pause:
                ret, self._frame = self._cap.read()
                if not ret:
                    self._cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
                    ret, self._frame = self._cap.read()
            if self._frame is not None:
                self._frame = cv2.resize(self._frame, (640, 480))

            self.get_object()

            key = cv2.waitKey(1)
            status = key_reader.get(key)
            if status is 'Toggle_pause':
                pause = not pause
            elif status is 'Exit':
                cv2.destroyAllWindows()
                break
            elif status is 'Parametros':
                t_parameters = not t_parameters
                savable = True
            elif status is 'Switch':
                switch = not switch
            elif status is 'Salvar' and savable:
                self.save_file()
            elif status is 'Ler':
                self.read_file()
                t_parameters = not t_parameters
                savable = True

            if self._frame is not None:
                if t_parameters:
                    self.parameters(switch)
                elif self._trackbar_startup:
                    self._trackbar_startup = False
                    cv2.destroyWindow('trackbar')
                    cv2.destroyWindow('Converted')

    def call(self, msg):
        msg = centraliza.msg.ObjectPos()
        msg.horiz = self._object_x / 640
        msg.vert = self._object_y / 480
        self._pub_img.publish(msg)

    def get_object(self):
        frame_tmp = self._frame.copy()
        # Encontra os contornos dos objetos filtrados na imagem
        contours, _ = cv2.findContours(self._mask_frame, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        contours = [cont for cont in contours if cv2.contourArea(cont) > 200]
        try:
            # Encontra o contorno com maior area
            biggest_contour = sorted(contours, key=lambda x: cv2.contourArea(x), reverse=True)[0]

            # Encontra o centroide do contorno com maior area
            moments = cv2.moments(biggest_contour)
            self._object_x = int(moments['m10'] / moments['m00'])
            self._object_y = int(moments['m01'] / moments['m00'])

            center, radius = cv2.minEnclosingCircle(biggest_contour)
            cv2.circle(frame_tmp, (int(center[0]), int(center[1])), int(radius), (0, 0, 255), thickness=3)

        except IndexError:
            pass

        if len(contours) == 0:
            self._object_x, self._object_y = -640, -480

        cv2.imshow('Result', frame_tmp)

    def parameters(self, switch):
        if not self._trackbar_startup:
            cv2.namedWindow('trackbar')
            cv2.createTrackbar('L_H', 'trackbar', 0, 255, self.lower_hue)
            cv2.createTrackbar('H_H', 'trackbar', 0, 255, self.higher_hue)
            cv2.createTrackbar('L_S', 'trackbar', 0, 255, self.lower_sat)
            cv2.createTrackbar('H_S', 'trackbar', 0, 255, self.higher_sat)
            cv2.createTrackbar('L_V', 'trackbar', 0, 255, self.lower_val)
            cv2.createTrackbar('H_V', 'trackbar', 0, 255, self.higher_val)

            cv2.setTrackbarPos('H_H', 'trackbar', self._high_h)
            cv2.setTrackbarPos('H_S', 'trackbar', self._high_s)
            cv2.setTrackbarPos('H_V', 'trackbar', self._high_v)
            cv2.setTrackbarPos('L_H', 'trackbar', self._low_h)
            cv2.setTrackbarPos('L_S', 'trackbar', self._low_s)
            cv2.setTrackbarPos('L_V', 'trackbar', self._low_v)
            self._trackbar_startup = True

        cvt_frame = cv2.cvtColor(self._frame, cv2.COLOR_BGR2HSV)

        min_hsv = np.array([self._low_h, self._low_s, self._low_v], np.uint8)
        max_hsv = np.array([self._high_h, self._high_s, self._high_v], np.uint8)
        self._mask_frame = cv2.inRange(cvt_frame, min_hsv, max_hsv)
        if switch:
            mask = cv2.bitwise_and(self._frame, self._frame, mask=self._mask_frame)
            cv2.imshow('Converted', mask)
        else:
            cv2.imshow('Converted', self._mask_frame)

    def lower_hue(self, value):
        self._low_h = value

    def higher_hue(self, value):
        self._high_h = value

    def lower_sat(self, value):
        self._low_s = value

    def higher_sat(self, value):
        self._high_s = value

    def lower_val(self, value):
        self._low_v = value

    def higher_val(self, value):
        self._high_v = value

    def save_file(self):
        data = {'min_h': self._low_h, 'max_h': self._high_h, 'min_s': self._low_s, 'max_s': self._high_s,
                'min_v': self._low_v, 'max_v': self._high_v}
        rospy.loginfo(f'Salvando {data}')
        file_location = os.path.abspath(__file__)
        file_location = file_location.split('/centraliza/')[0]
        with open(f'{file_location}/centraliza/files/image_parameters.json', 'r') as file:
            json.dump(data, file)

    def read_file(self):
        file_location = os.path.abspath(__file__)
        file_location = file_location.split('/centraliza/')[0]
        with open(f'{file_location}/centraliza/files/image_parameters.json', 'r') as file:
            data = json.load(file)
            self._low_h = data.get('min_h', 0)
            self._high_h = data.get('max_h', 255)
            self._low_s = data.get('min_s', 0)
            self._high_s = data.get('max_s', 255)
            self._low_v = data.get('min_v', 0)
            self._high_v = data.get('max_v', 255)


def main(*args):
    cam = Camera()
    cam.open()
    cam.read('')
    rospy.spin()
