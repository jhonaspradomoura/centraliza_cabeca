from neurones.generic import Neuron
from utilities.transfer import Linear


class Exit(Neuron):
	def __init__(self, data=None, bias=0, transfer=Linear()):
		super().__init__(data=data, bias=bias, transfer=transfer)
	
	def add_forward_weight(self, **kwargs):
		raise AttributeError('Neuronio de saida nao deve ter neuronios apos ele.')
	
	def out(self):
		return self.data, self.objective
	
	def local_gradient(self, lr, error=None):
		assert error is not None, 'O neuronio de saida deve receber um erro para comecar a backpropagation.'
		self._gradient = -error[0] * self._transfer.backward(self._activation_value_h), \
						 -error[1] * self._transfer.backward(self._activation_value_v)
		return self._gradient
