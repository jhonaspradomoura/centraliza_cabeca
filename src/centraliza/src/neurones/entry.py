from neurones.generic import Neuron
from utilities.transfer import Linear


class Entry(Neuron):
	def __init__(self, data=None, obj=(.5, .5), bias=0, transfer=Linear()):
		super().__init__(data=data, bias=bias, transfer=transfer)
		self.objective = obj
	
	def update(self, data):
		assert isinstance(data, tuple) and len(data) == 2, f'data deve ser uma tuple e nao um {type(data)}, ' \
			f'e deve ser tamanho 2, e nao {len(data)}'
		self.data = data
	
	def activation_function(self):
		return False
	
	def add_origin_weight(self, weight):
		raise AttributeError('Neuronio de entrada nao pode ter neuronio de origem.')
