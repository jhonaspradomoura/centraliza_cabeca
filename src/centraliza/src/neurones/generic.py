from utilities.transfer import Linear


class Neuron(object):
	def __init__(self, data=None, *args, **kwargs):
		self._bias = kwargs.get('bias', 0)
		self._transfer = kwargs.get('transfer', Linear())
		self.__data = data
		self.__objective = None
		self._activation_value_h = 0
		self._activation_value_v = 0
		
		self._gradient = (0, 0)
		self._weights_origin = set()
		self._weights_forward = set()
	
	def __dict__(self):
		return {'bias': self._bias, 'transfer': self._transfer.__class__.__name__}
	
	@property
	def data(self):
		return self.__data
	
	@data.setter
	def data(self, value: dict):
		assert isinstance(value, tuple) and len(value) == 2, f'data deve ser uma tuple, e nao um {type(value)},' \
			f' deve estar na ordem: obj_h, obj_v'
		self.__data = value
	
	@property
	def objective(self):
		return self.__objective
	
	@objective.setter
	def objective(self, value):
		assert (isinstance(value, tuple) and len(value) == 2), f'objective deve ser uma tuple, e nao um ' \
			f'{type(value)}, deve estar na ordem: obj_h, obj_v'
		self.__objective = value
	
	@property
	def weights_origin(self):
		return self._weights_origin.copy()
	
	@property
	def weights_forward(self):
		return self._weights_forward.copy()
	
	@property
	def gradient(self):
		return self._gradient
	
	def add_origin_weight(self, weight):
		self._weights_origin.add(weight)
	
	def add_forward_weight(self, weight):
		self._weights_forward.add(weight)
	
	def clear_weights(self):
		self._weights_forward.clear()
		self._weights_origin.clear()
	
	def activation_function(self):
		self._activation_value_h = 0
		self._activation_value_v = 0
		for weight in self._weights_origin:
			self._activation_value_h += weight.data[0] * weight.value[0] + self._bias
			self._activation_value_v += weight.data[1] * weight.value[1] + self._bias
		self.transfer_function()
	
	def transfer_function(self):
		self.__data = self._transfer.forward(self._activation_value_h), \
					  self._transfer.forward(self._activation_value_v)
	
	def forward(self):
		return self._weights_forward.copy()
	
	def backward(self, lr, error=None):
		self.local_gradient(lr, error)
		return self.weights_origin
	
	def local_gradient(self, lr, error=None):
		sum_of_fwd_effects = [0, 0]
		for fwd_connection in self.weights_forward:
			effect = fwd_connection.effect_on_fwd_neurones()
			sum_of_fwd_effects[0] += effect[0]
			sum_of_fwd_effects[1] += effect[1]
		
		self._gradient = (-lr * self._transfer.backward(self._activation_value_h) + sum_of_fwd_effects[0],
						  -lr * self._transfer.backward(self._activation_value_v) + sum_of_fwd_effects[1])
