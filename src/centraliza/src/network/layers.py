class Layer(list):
	def __init__(self, neuron_type, neuron=None):
		super().__init__()
		self._nt = neuron_type
		if neuron:
			self.append(neuron)
	
	@property
	def type(self):
		return self._nt.__name__
	
	def append(self, neuron):
		assert isinstance(neuron, self._nt), f'neuron deve ser do tipo {self._nt}'
		super(Layer, self).append(neuron)
	
	def add_n(self, n, *args, **kwargs):
		for _ in range(n):
			self.append(self._nt(*args, **kwargs))
	
	def change_objective(self, obj):
		for neuron in self:
			neuron.objective = obj
	
	def output(self):
		try:
			outs = []
			for neuron in self:
				outs.append(neuron.out)
			return outs
		except AttributeError:
			return None
