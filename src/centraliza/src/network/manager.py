import itertools
import json
import os
import shutil
import threading
import time

import centraliza.msg
import rospy
import std_msgs.msg

from network.layers import Layer
from neurones.entry import Entry
from neurones.exit import Exit
from neurones.generic import Neuron
from utilities.transfer import Linear, NSigmoid, Sigmoid, Tanh
from utilities.weights import Weight


# todo: adicionar salvamento da rede neural e carregamento de uma rede salva.

class Network(object):
    def __init__(self):
        self._layers = {1: Layer(Entry), 2: Layer(Exit)}
        rospy.init_node('manager', anonymous=True, log_level=rospy.DEBUG)

        # Variaveis com a posicao do objeto
        self._obj_hor, self._obj_vert = None, None

        # Taxa de aprendizagem
        self._lr = 0.1

        # Pesos
        self._weights = {}

        # Ciclo de nomes para o arquivo salvo.
        self._save_iter = itertools.cycle(range(5))

        self._pub_mot_origin = rospy.Publisher('motor_return_origin', std_msgs.msg.Empty, queue_size=1)

        self._pub_mot = rospy.Publisher('network', centraliza.msg.Output, queue_size=1, latch=True)
        self._sub_mot = rospy.Subscriber('mov_status', std_msgs.msg.Bool, callback=self._update_status, queue_size=1)

        self._pub_cam = rospy.Publisher('camera_cb', std_msgs.msg.Empty, queue_size=1, latch=True)
        self._sub_cam = rospy.Subscriber('camera_fb', centraliza.msg.ObjectPos, queue_size=1,
                                         callback=self._update_img)

        self._done_moving = threading.Event()
        self._done_updating_img = threading.Event()
        rospy.on_shutdown(self.__del__)

    def __del__(self):
        self.save()
        self._done_moving.set()
        self._done_updating_img.set()

    def __setitem__(self, layer, neuron):
        if self._layers.get(layer):
            if 1 < layer < len(self._layers):
                self._layers[layer].append(neuron)
            elif layer == 1:
                self._update_layers(2)
                self[layer + 1] = Layer(neuron_type=type(neuron), neuron=neuron)
            elif layer == len(self._layers):
                self._update_layers(layer)
                self[layer - 1] = Layer(neuron_type=type(neuron), neuron=neuron)
            else:
                raise ValueError('nao é possivel adicionar layer menor que 1 ou maior que o tamanho da rede.')
        else:
            self._layers[layer] = Layer(neuron_type=type(neuron), neuron=neuron)

    def __getitem__(self, layer):
        return self._layers[layer]

    def __len__(self):
        return len(self._layers)

    def __reversed__(self):
        return reversed(sorted(self._layers.items(), key=lambda x: x[0]))

    def _update_layers(self, layer_pos):
        if layer_pos == 1:
            layer_pos = 2
        for layer in reversed(sorted(list(self._layers.keys()))[layer_pos - 1:]):
            self._layers[layer + 1] = self._layers[layer]
        self._layers.pop(layer_pos)
        return layer_pos

    def add_hidden_layer(self, layer=-1, n=1, *args, **kwargs):
        if layer == -1:
            layer = len(self)
        layer = self._update_layers(layer)
        self._layers[layer] = Layer(Neuron)
        self.add_n_neurons(layer, n, *args, **kwargs)

    def add_entry_neuron(self, neuron):
        self._layers[1].append(neuron)

    def add_exit_neuron(self, neuron):
        self._layers[len(self)].append(neuron)

    def add_n_neurons(self, layer, n, *args, **kwargs):
        self[layer].add_n(n, *args, **kwargs)

    def connect_layers(self):
        sorted_keys = sorted(self._layers.keys())
        for layer_pair in zip(sorted_keys, sorted_keys[1:]):
            neuron_pairs = itertools.product(self._layers[layer_pair[0]], self._layers[layer_pair[1]])
            id_origin, id_dest, last_neuron_origin, last_neuron_dest = -1, 0, None, None

            for neuron_pair in neuron_pairs:
                if neuron_pair[0] != last_neuron_origin:
                    id_origin += 1
                    id_dest = 0
                elif neuron_pair[1] != last_neuron_dest:
                    id_dest += 1
                last_neuron_origin = neuron_pair[0]
                last_neuron_dest = neuron_pair[1]

                self._connect(neuron_pair[0], neuron_pair[1], int(f'{layer_pair[0]}{id_origin}'),
                              int(f'{layer_pair[1]}{id_dest}'))

    def _connect(self, original_neuron: Neuron, next_neuron: Neuron, id_origin, id_dest, random=True):
        if not original_neuron.weights_forward.intersection(next_neuron.weights_origin):
            wgt = Weight(original_neuron, next_neuron, id_origin, id_dest, random)
            self._weights.update({(id_origin, id_dest): wgt})

    def _get_entry_data(self):
        while not rospy.is_shutdown():
            self._pub_cam.publish(std_msgs.msg.Empty())

            self._done_updating_img.wait()

            # Caso o objeto não esteja na imagem, deve pedir para o operador mover a camera
            if self._obj_hor == -1 or self._obj_vert == -1:
                input('Mova o objeto ate que ele esteja sendo reconhecido, depois pressione ENTER para '
                      'continuar\n')
            else:
                for neuron in self._layers[1]:
                    neuron.update((self._obj_hor, self._obj_vert))
                break

    def run(self, train=False, objective=(.5, .5)):
        self._get_entry_data()
        rospy.logdebug(f'Entry data - {self._obj_hor, self._obj_vert}')

        sorted_items = sorted(self._layers.items(), key=lambda x: x[0])
        self._layers[1].change_objective(objective)

        self.connect_layers()
        rospy.logdebug('Connected layer successfully')

        for layer, neurons in sorted_items:
            weights_set = set()
            act_functions = set()
            for neuron in neurons:
                weights_set.update(neuron.forward())
            for weight in weights_set:
                act_functions.add(weight.forward())
            funcs = map(lambda fct: fct, act_functions)
            for func in funcs:
                func()
        else:
            output = self[len(self)][0].out()

        rospy.logdebug(f'Output calculated - {output[0], output[1]}')

        # Envia output para os motores
        msg = centraliza.msg.Output()
        msg.horiz, msg.vert = output[0]
        self._done_moving.clear()
        self._pub_mot.publish(msg)

        if train:
            self.evaluate(output)

            self._get_entry_data()

            self.save_tmp()
            time.sleep(0.2)
            self._done_moving.clear()
            self._pub_mot_origin.publish(std_msgs.msg.Empty())
            self._done_moving.wait()
            self.run(train=train, objective=objective)

    def evaluate(self, output):
        """
        Função responsável por ler o output, decidir qual ação escolher (no caso so tera uma opção), mandar o robo
        agir, ler seu feedback sobre a eficácia da ação, e calcular o erro
        :param output:
        :return:
        """
        rospy.logdebug('Evaluation started.')

        # Espera pela resposta de movimento concluido, callback na função _update_status
        self._done_moving.wait()
        rospy.logdebug('Move done')

        # Requisita a nova posição do objeto
        self._done_updating_img.clear()
        self._pub_cam.publish(std_msgs.msg.Empty())

        # Verificar a atualizacao da posicao na imagem, callback na função _update_img
        self._done_updating_img.wait()
        rospy.logdebug('Image updated.')

        # Verifica se o objeto chegou a ser encontrado, se nao, requisita um input do operador
        if self._obj_hor == -1 or self._obj_vert == -1:
            ans = input('O objeto nao foi encontrado, aplique o erro de acordo com a maior distancia em relação ao '
                        'movimento da cabeça? Sendo:\n1 - diagonal superior esquerda, 2 - diagonal inferior esquerda, '
                        '3 - diagonal superior direita, 4 - diagonal inferior direita\n')
            rospy.logdebug(ans)
            dec_table = {1: (-.1, -.1), 2: (-.1, 1.1), 3: (1.1, -.1), 4: (1.1, 1.1)}
            self._obj_hor, self._obj_vert = dec_table.get(int(ans), (-.1, -.1))
        error = self._calc_error(output[1])
        rospy.logdebug(f'Error was {error}')

        # Backpropagate
        self.backprop(error)

    def _update_status(self, status):
        if status:
            self._done_moving.set()
        else:
            self._done_moving.clear()

    def _update_img(self, msg):
        self._obj_hor = msg.horiz
        self._obj_vert = msg.vert

        self._done_updating_img.set()

    def _calc_error(self, objective):
        rospy.logdebug(f'Calculando erro, obj = {objective}, posicao = {self._obj_hor, self._obj_vert}')
        error_x = (objective[0] - self._obj_hor)
        error_y = (objective[1] - self._obj_vert)
        return error_x, error_y

    def backprop(self, error):
        rospy.logdebug('Starting backpropagation.')
        for layer, items in reversed(self):
            for neuron in items:
                weights = neuron.backward(self._lr, error)
                for weight in weights:
                    weight.backward()

    def save(self):
        last_file = next(self._save_iter) - 1
        if last_file == -1:
            last_file = 4
        ans = input(f'Gostaria de salvar qual arquivo temporario no final, aperte N para cancelar? 0, 1, 2, '
                    f'3 ou 4? [{last_file}]')
        file_location = os.path.abspath(__file__)
        file_location = file_location.split('/centraliza/')[0]
        if ans in ('', 0, 1, 2, 3, 4):
            if ans not in (0, 1, 2, 3, 4):
                ans = last_file
            shutil.copy(f'{file_location}/centraliza/files/network_{ans}.json',
                        f'{file_location}/centraliza/files/network.json')
        else:
            pass

    def save_tmp(self):
        data = {'Layers': {}, 'Weights': {}}
        for _id, layer in self._layers.items():
            data['Layers'].update({_id: [layer.type, len(self._layers[_id]), layer[0].__dict__()]})
        for ids, weight_obj in self._weights.items():
            data['Weights'].update({str(ids): str(weight_obj)})

        file_location = os.path.abspath(__file__)
        file_location = file_location.split('/centraliza/')[0]
        with open(f'{file_location}/centraliza/files/network_{next(self._save_iter)}.json', 'w') as file:
            json.dump(data, file)

    def load(self):
        self._layers.clear()
        file_location = os.path.abspath(__file__)
        file_location = file_location.split('/centraliza/')[0]
        with open(f'{file_location}/centraliza/files/network.json', 'r') as file:
            data = json.load(file)
            layers = data.get('Layers')
            weights = data.get('Weights')

        for _id, info in layers.items():
            assert info[2]['transfer'] in (Sigmoid.__name__, Linear.__name__, NSigmoid.__name__, Tanh.__name__)
            layer = Layer(eval(info[0]))
            bias = info[2]['bias']
            transfer = eval(info[2]['transfer'])

            if transfer is not NSigmoid:
                layer.add_n(info[1], bias=bias, transfer=transfer())
            else:
                layer.add_n(info[1], bias=bias, transfer=transfer(interval=(-1, 1)))
            self._layers.update({int(_id): layer})

        self.connect_layers()

        for key, value in weights.items():
            key = eval(key)
            value = eval(value)
            weight = self._weights[key]
            weight.value = value
        for keys in self._weights.keys():
            rospy.logerr(f'{keys}: {str(self._weights[keys])}')


def main():
    net = Network()
    net.load()
    # net.add_entry_neuron(Entry(transfer=Sigmoid()))
    # net.add_hidden_layer(n=3, transfer=Tanh())
    # net.add_hidden_layer(n=3, transfer=Tanh())
    # net.add_hidden_layer(n=3, transfer=Tanh())
    # net.add_exit_neuron(Exit(transfer=NSigmoid((-1, 1))))
    net.run(train=True)
    rospy.spin()
