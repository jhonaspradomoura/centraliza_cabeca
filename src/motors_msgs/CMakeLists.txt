cmake_minimum_required(VERSION 2.8.3)
project(motors_msgs)


find_package(catkin REQUIRED COMPONENTS
  message_generation
  std_msgs 
  
)

add_message_files(
  FILES
  motors_msgs.msg
)
generate_messages(
  DEPENDENCIES
  std_msgs
)

catkin_package(
  # INCLUDE_DIRS include
  CATKIN_DEPENDS message_generation std_msgs  
)

include_directories(
  # include
  ${catkin_INCLUDE_DIRS}
)

