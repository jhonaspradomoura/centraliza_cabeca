#include <DynamixelWorkbench.h>

int main(int argc, char *argv[]) 
{
  const char* port_name = "/dev/ttyUSB0";
  int baud_rate = 1000000;
  int dxl_id_1 = 1;
  int dxl_id_2 = 2;
  int position, position_2;
  int repeticoes;

  printf("Digite a posicao desejada do motor de ID 1 : [ 0 ate 512 ]\n");
  scanf("%i", &position);

  printf("\n Digite a posicao desejada do motor de ID 2 : [ 0 ate 512 ]\n");
  scanf("%i", &position_2);

   sleep(4);

  /*printf("Digite o numero de repeticoes \n: [ 0 ate 512 ]\n");
  scanf("%i", &repeticoes);*/

  DynamixelWorkbench dxl_wb;

  const char *log;
  bool result = false;
  bool result_2 = false;

  uint16_t model_number = 0;
  

  result = dxl_wb.init(port_name, baud_rate, &log);
  if (result == false)
  {
    printf("%s\n", log);
    printf("Failed to init\n");

    return 0;
  }
  else
    printf("\n Succeed to init(%d)\n", baud_rate);  

  result = dxl_wb.ping(dxl_id_1, &model_number, &log);
  //result_2 = dxl_wb.ping(dxl_id_2, &model_number, &log);
  if ((result == false))// || (result_2 == false)) 
  {
    printf("%s\n", log);
    printf("Failed to ping\n");
  }
  else
  {
    printf("\n Succeed to ping\n");
    printf("id_1 : %d, model_number : %d\n", dxl_id_1, model_number);
    //printf("id_2 : %d, model_number : %d\n", dxl_id_2, model_number);

  }

  result = dxl_wb.jointMode(dxl_id_1, 50, 0, &log);
  //result_2 = dxl_wb.jointMode(dxl_id_2, 50, 0, &log);

  if (result == false)
  {
    printf("%s\n", log);
    printf("Failed to change joint mode\n");
  }
  else
  {
    printf("Succeed to change joint mode\n");
    printf("Dynamixel is moving...\n");

    for (int count = 0; count < 2; count++)
    {
      dxl_wb.goalPosition(dxl_id_1, position);
     //dxl_wb.goalPosition(dxl_id_2, position_2);
      sleep(2);
      /*
      dxl_wb.goalPosition(dxl_id_1, 0);
      dxl_wb.goalPosition(dxl_id_2, 0);
      sleep(2);*/
    }
  }

  return 0;
}
