#include <DynamixelWorkbench.h>

int main(int argc, char *argv[]) 
{
  const char* port_name = "/dev/ttyUSB0";
  int baud_rate = 1000000;
  int dxl_id_1 = 1;
  int dxl_id_2 = 2;
  int position, position_2;
  int repeticoes;

  DynamixelWorkbench dxl_wb;

  const char *log;
  bool result = false;
  bool result_2 = false;
  
  uint16_t model_number = 0;
  int32_t get_data = 0;


  result = dxl_wb.init(port_name, baud_rate, &log);
  if (result == false)
  {
    printf("%s\n", log);
    printf("Falha ao iniciar. \n");

    return 0;
  }
  else
    printf("\n Sucesso ao iniciar. (%d)\n", baud_rate);  

  result = dxl_wb.ping(dxl_id_1, &model_number, &log);
  result_2 = dxl_wb.ping(dxl_id_2, &model_number, &log);

  if ((result == false) || (result == false)) 
  {
    printf("%s\n", log);
    printf("Falha ao dar Ping. \n");
  }
  else
  {
    printf("\nMotores Decectados. \n");
    printf("   id_1 : %d, model_number : %d\n", dxl_id_1, model_number);
    printf("   id_2 : %d, model_number : %d\n", dxl_id_2, model_number);

  }

  result = false;//dxl_wb.torqueOff(dxl_id_1, &log);
   if (result == false)
    {
    result = dxl_wb.torque(dxl_id_1, 0, &log);
    printf("\nTorque Desativado com sucesso. ");
    printf("\n\n");
    }
   else
   {
    result = dxl_wb.torque(dxl_id_1, 1, &log);   
    printf("ERRO 404 %d\n", result);
   }

   result = dxl_wb.jointMode(dxl_id_2, 0, 0, &log);

   while (result == true)
   {
    result = dxl_wb.jointMode(dxl_id_2, 0, 0, &log);
    printf("Posicao Atual do motor :");
    position = dxl_wb.readRegister(dxl_id_1, "Present_Position", &get_data, &log);
       dxl_wb.goalPosition(dxl_id_2, get_data);
       printf("%d\n", get_data);

       //dxl_wb.goalPosition(dxl_id_2, position_2);
       /*
       dxl_wb.goalPosition(dxl_id_1, 0);
       dxl_wb.goalPosition(dxl_id_2, 0);
       sleep(2);*/
    }
    if (result == false)
    {
        printf("ERROR 404. ");
    }
    
return 0;

}