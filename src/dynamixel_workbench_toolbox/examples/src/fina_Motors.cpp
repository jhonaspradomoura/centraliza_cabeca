#include <DynamixelWorkbench.h>

int main(int argc, char *argv[]) 
{
  const char* port_name = "/dev/ttyUSB0";
  int baud_rate = 1000000;
  int dxl_id = 1;
  
  DynamixelWorkbench dxl_wb;

  const char *log;
  bool result = false;

  uint8_t scanned_id[16];
  uint8_t dxl_cnt = 0;
  uint8_t range = 100;
 


  result = dxl_wb.init(port_name, baud_rate, &log);
  if (result == false)
  {
    printf("%s\n", log);
    printf("Failed to init\n");

    return 0;
  }
  else
    printf("Succeeded to init(%d)\n", baud_rate);  

  printf("Wait for scan...\n");
  result = dxl_wb.scan(scanned_id, &dxl_cnt, range, &log);
  if (result == false)
  {
    printf("%s\n", log);
    printf("Failed to scan\n");
  }
  else
  {
    printf("Find %d Dynamixels\n", dxl_cnt);

    for (int cnt = 0; cnt < dxl_cnt; cnt++)
      printf("id : %d, model name : %s\n", scanned_id[cnt], dxl_wb.getModelName(scanned_id[cnt]));
  }

uint16_t model_number = 0;


  result = dxl_wb.wheelMode(scanned_id[0], 0, &log);
  if (result == false)
  {
    printf("%s\n", log);
    printf("Failed to change wheel mode\n");
  }
  else
  {
    printf("Succeed to change wheel mode\n");
    printf("Dynamixel is moving...\n");

    for (int count = 0; count < 3; count++)
    {
      dxl_wb.goalVelocity(dxl_id, (int32_t)-100);
      sleep(3);

      dxl_wb.goalVelocity(dxl_id, (int32_t)100);
      sleep(3);
    }

    dxl_wb.goalVelocity(dxl_id, (int32_t)0);
  }

  return 0;


}
