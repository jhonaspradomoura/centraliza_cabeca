#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/hericles/Documents/edrom_Documentos/Teste_Lorena/dynamixel-workbench-2.0.0/dynamixel_workbench_toolbox/examples/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/hericles/Documents/edrom_Documentos/Teste_Lorena/dynamixel-workbench-2.0.0/dynamixel_workbench_toolbox/examples/devel/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/hericles/Documents/edrom_Documentos/Teste_Lorena/dynamixel-workbench-2.0.0/dynamixel_workbench_toolbox/examples/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PYTHONPATH="/home/hericles/trainee/devel/lib/python2.7/dist-packages:/home/hericles/edrom/devel/lib/python2.7/dist-packages:/opt/ros/kinetic/lib/python2.7/dist-packages:/home/hericles/edrom//src/vision/object_finder//models/research/slim"
export ROSLISP_PACKAGE_DIRECTORIES="/home/hericles/Documents/edrom_Documentos/Teste_Lorena/dynamixel-workbench-2.0.0/dynamixel_workbench_toolbox/examples/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/hericles/Documents/edrom_Documentos/Teste_Lorena/dynamixel-workbench-2.0.0/dynamixel_workbench_toolbox:$ROS_PACKAGE_PATH"